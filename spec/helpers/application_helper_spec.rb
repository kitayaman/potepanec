require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe "headのtitle" do
    subject { full_title(page_title) }

    context "page_titleが空の場合" do
      let(:page_title) { "" }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "page_titleがnilの場合" do
      let(:page_title) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "page_titleに値(Test Bag)がある場合" do
      let(:page_title) { "Test Bag" }

      it { is_expected.to eq 'Test Bag - BIGBAG Store' }
    end
  end
end
