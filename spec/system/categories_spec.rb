require 'rails_helper'

RSpec.describe 'categories system_spec', type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:bags_taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:mugs_taxon) { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:tote_product) { create(:product, name: "Tote", price: 30, taxons: [bags_taxon]) }
  let(:stein_product) { create(:product, name: "Stein", price: 60, taxons: [mugs_taxon]) }
  let(:image1) { build(:image) }
  let(:image2) { build(:image) }

  before do
    tote_product.images << image1
    stein_product.images << image2
    visit potepan_category_path(bags_taxon.id)
  end

  describe "商品カテゴリーの表示" do
    it "taxonomy（大分類）が表示されること" do
      expect(page).to have_selector 'a[data-toggle=collapse]', text: taxonomy.name
    end

    it "taxon（小分類）が表示されること" do
      expect(page).to have_title "#{bags_taxon.name} - BIGBAG Store"
      expect(page).to have_selector '.page-title h2', text: bags_taxon.name
      expect(page).to have_selector '.breadcrumb li', text: bags_taxon.name
      within ".collapseItem" do
        expect(page).to have_content bags_taxon.name
        expect(page).to have_content mugs_taxon.name
      end
    end

    it "選んだtaxonのproduct（商品）が表示されること" do
      expect(page).to have_selector '.productCaption', text: tote_product.name
      expect(page).to have_selector '.productCaption', text: tote_product.display_price.to_s
    end

    it "選んだtaxonのproduct（商品）の画像が表示されること" do
      expect(page).to have_selector("img[alt='#{tote_product.name} の画像']")
    end

    it "選んだtaxonのproduct（商品）の数だけ表示されること" do
      expect(page).to have_selector('.productBox', count: bags_taxon.all_products.count)
    end

    it "選んだtaxonの以外のproduct（商品）が表示されないこと" do
      expect(page).not_to have_selector '.productCaption', text: stein_product.name
      expect(page).not_to have_selector '.productCaption', text: stein_product.display_price.to_s
    end
  end

  describe "プルダウンの開閉動作" do
    context "プルダウンが閉じている時" do
      it "navbarのulのクラスがcallapseだけになっていること" do
        within ".side-nav" do
          expect(page).to have_css '.collapse'
          expect(page).not_to have_css '.in'
        end
      end
    end

    context "プルダウンが開いている時" do
      it "navbarのulのクラスにinが追加されていること", js: true do
        find('a', text: "#{taxonomy.name}").click
        within ".side-nav" do
          expect(page).to have_css '.collapse'
          expect(page).to have_css '.in'
        end
      end
    end
  end

  describe "ページ遷移" do
    it "カテゴリーページからトップページへ遷移できること" do
      first(:link, 'Home').click
      expect(current_path).to eq potepan_path
      expect(page).to have_title "BIGBAG Store"
    end

    it "product（商品）から商品詳細ページへ遷移できること" do
      click_link tote_product.name
      expect(current_path).to eq potepan_product_path(tote_product.id)
      expect(page).to have_title "#{tote_product.name} - BIGBAG Store"
      expect(page).to have_selector '.page-title h2', text: tote_product.name
      expect(page).to have_selector '.breadcrumb li', text: tote_product.name
      expect(page).to have_selector("img[alt='#{tote_product.name} の画像']")
      within '.media-body' do
        expect(page).to have_selector 'h2', text: tote_product.name
        expect(page).to have_selector 'h3', text: tote_product.display_price.to_s
        expect(page).to have_selector 'p', text: tote_product.description
      end
    end

    it "別のproduct（商品）を選択して、商品詳細ページへ遷移できること", js: true do
      find('a', text: "#{taxonomy.name}").click
      find('a', text: "#{mugs_taxon.name}").click
      click_link stein_product.name
      expect(current_path).to eq potepan_product_path(stein_product.id)
      expect(page).to have_title "#{stein_product.name} - BIGBAG Store"
      expect(page).to have_selector '.page-title h2', text: stein_product.name.upcase
      expect(page).to have_selector '.breadcrumb li', text: stein_product.name.upcase
      expect(page).to have_selector("img[alt='#{stein_product.name} の画像']")
      within '.media-body' do
        expect(page).to have_selector 'h2', text: stein_product.name.upcase
        expect(page).to have_selector 'h3', text: stein_product.display_price.to_s
        expect(page).to have_selector 'p', text: stein_product.description
      end
    end
  end
end
