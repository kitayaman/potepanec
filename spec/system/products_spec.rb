require 'rails_helper'

RSpec.describe 'products system_spec', type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product) { create(:product, name: "Tote", price: 30, taxons: [taxon]) }
  let(:related_product) { create(:product, name: "Backpack", price: 45, taxons: [taxon]) }
  let(:product_image) { build(:image) }
  let(:related_image) { build(:image) }

  before do
    product.images << product_image
    related_product.images << related_image
    visit potepan_product_url(product.id)
  end

  describe "カテゴリーページへの遷移" do
    it "カテゴリーページへ遷移できること" do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    end

    it "カテゴリーページが正しく表示されること" do
      click_on '一覧ページへ戻る'
      expect(page).to have_selector 'a[data-toggle=collapse]', text: taxonomy.name
      expect(page).to have_selector '.page-title h2', text: taxon.name
      expect(page).to have_selector '.breadcrumb li', text: taxon.name
      expect(page).to have_selector '.collapseItem', text: taxon.name
      expect(page).to have_selector '.productCaption', text: product.name
      expect(page).to have_selector '.productCaption', text: product.display_price.to_s
      expect(page).to have_selector("img[alt='#{product.name} の画像']")
      expect(page).to have_selector('.productBox', count: taxon.all_products.count)
    end
  end

  describe "関連商品の遷移" do
    it "関連商品の詳細ページに遷移できること" do
      click_on related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
      expect(page).to have_title "#{related_product.name} - BIGBAG Store"
    end

    it "関連商品の詳細ページが正しく表示されていること" do
      click_on related_product.name
      expect(page).to have_selector '.page-title h2', text: related_product.name
      expect(page).to have_selector '.breadcrumb li', text: related_product.name
      expect(page).to have_selector("img[alt='#{related_product.name} の画像']")
      within '.media-body' do
        expect(page).to have_selector 'h2', text: related_product.name
        expect(page).to have_selector 'h3', text: related_product.display_price.to_s
        expect(page).to have_selector 'p', text: related_product.description
      end
      expect(page).to have_selector("img[alt='#{product.name} の画像']")
      within '.productCaption' do
        expect(page).to have_selector 'h5', text: product.name
        expect(page).to have_selector 'h3', text: product.display_price.to_s
      end
    end
  end
end
