require 'rails_helper'

RSpec.describe 'products_decorator_spec', type: :model do
  let(:taxon) { create(:taxon, name: "Bags") }
  let(:other_taxon) { create(:taxon, name: "Other") }
  let(:product) { create(:product, name: "Tote", taxons: [taxon]) }
  let(:related_product1) { create(:product, name: "Bag1", taxons: [taxon]) }
  let(:related_product2) { create(:product, name: "Bag2", taxons: [taxon]) }
  let(:other_product1) { create(:product, name: "Pen1", taxons: [other_taxon]) }

  describe "related_products" do
    it "関連商品のみが取得できること" do
      expect(product.related_products).to match_array([related_product1, related_product2])
    end
  end
end
