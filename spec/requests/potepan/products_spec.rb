require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "商品詳細ページにアクセス" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:other_taxon) { create(:taxon, name: "Other", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, name: "Tote", price: 99, taxons: [taxon]) }
    let(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let(:other_product) { create(:product, name: "PS4", price: 400, taxons: [other_taxon]) }
    let(:product_image) { build(:image) }
    let(:related_images) { build_list(:image, 5) }
    let(:other_image) { build(:image) }

    before do
      product.images << product_image
      related_products.each_with_index do |related_product, i|
        related_products[i].images << related_images[i]
      end
      get potepan_product_url(product.id)
    end

    describe "レスポンス" do
      it "正常なレスポンスか返ってくること" do
        expect(response).to be_successful
      end

      it "200レスポンスが返ってくること" do
        expect(response).to have_http_status(200)
      end
    end

    describe "テンプレート" do
      it "headerがレンダリングされていること" do
        expect(response.body).to include("検索")
      end

      it "light_sectionがレンダリングされていること" do
        expect(response.body).to include("Home")
      end

      it "related_productsがレンダリングされていること" do
        expect(response.body).to include("関連商品")
      end

      it "footerがレンダリングされていること" do
        expect(response.body).to include("Copyright Potepan Store .")
      end
    end

    describe "productパラメーター" do
      it "productを取得できていること" do
        expect(response.body).to include(product.name)
        expect(response.body).to include(product.display_price.to_s)
        expect(response.body).to include(product.description)
        expect(response.body).to include(product.images.first.attachment(:large))
      end
    end

    describe "related_productsパラメーター" do
      it "１〜４つ目のrelated_productを取得できていること" do
        4.times do |i|
          expect(response.body).to include(related_products[i].name)
          expect(response.body).to include(related_products[i].display_price.to_s)
          expect(response.body).to include(related_products[i].images.first.attachment(:small))
        end
      end

      it "５つ目のrelated_productは取得できていないこと" do
        # related_productsのpriceのテストデータが全て同じな為、priceを除いたテストでパラメータが未取得だと判断しています。
        expect(response.body).not_to include(related_products[4].name)
        expect(response.body).not_to include(related_products[4].images.first.attachment(:small))
      end
    end

    describe "other_productsパラメーター" do
      it "other_productの情報が表示されないこと" do
        other_product.images << other_image
        expect(response.body).not_to include(other_product.name)
        expect(response.body).not_to include(other_product.display_price.to_s)
        expect(response.body).not_to include(other_product.images.first.attachment(:small))
      end
    end
  end
end
