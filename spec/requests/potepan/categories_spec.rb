require 'rails_helper'

RSpec.describe "categories", type: :request do
  describe "カテゴリーページにアクセス" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, parent: taxonomy.root) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:image) { build(:image) }

    before do
      product.images << image
      get potepan_category_path(taxon.id)
    end

    describe "レスポンス" do
      it "正常なレスポンスか返ってくること" do
        expect(response).to be_successful
      end

      it "200レスポンスが返ってくること" do
        expect(response).to have_http_status(200)
      end
    end

    describe "テンプレート" do
      it "light_sectionがレンダリングされていること" do
        expect(response.body).to include(taxon.name)
      end

      it "left_panelがレンダリングされていること" do
        expect(response.body).to include("商品カテゴリー")
      end

      it "filter_areaがレンダリングされていること" do
        expect(response.body).to include("並び替え")
      end
    end

    describe "パラメーター" do
      it "product.nameを取得できていること" do
        expect(response.body).to include(product.name)
      end

      it "product.display_priceを取得できていること" do
        expect(response.body).to include(product.display_price.to_s)
      end

      it "product.images.first.attatchment(:product)を取得できていること" do
        expect(response.body).to include(product.images.first.attachment(:product))
      end

      it "taxonomy.nameを取得できていること" do
        expect(response.body).to include(taxonomy.name)
      end
    end
  end
end
