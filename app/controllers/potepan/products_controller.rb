class Potepan::ProductsController < ApplicationController
  MAX_NUM_OF_RELATED_PRODUCTS_DISPLAYED = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:images, :default_price]).limit(MAX_NUM_OF_RELATED_PRODUCTS_DISPLAYED)
  end
end
